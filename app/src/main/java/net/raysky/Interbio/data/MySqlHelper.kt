package net.raysky.Interbio.data

import android.annotation.SuppressLint
import android.content.ContentValues
import android.database.sqlite.SQLiteOpenHelper
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.widget.Toast

class MySqlHelper(var context: Context, private var DB_VERSION: Int = CURRENT_VERSION)
    : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    companion object {
        const val DB_NAME = "user.db"
        const val TABLE_NAME = "user_info"
        const val CURRENT_VERSION = 1

        const val COL_ID = "id"
        const val COL_NAME = "name"
        const val COL_PHOTO = "photo"
        const val COL_FACTION = "faction"
        const val COL_TIME = "time"

        @SuppressLint("StaticFieldLeak")
        private var instance: MySqlHelper? = null

        @Synchronized
        fun getInstance(ctx: Context, version: Int = 0): MySqlHelper {
            if (instance == null) {
                instance = if (version > 0) MySqlHelper(ctx.applicationContext, version)
                else {
                    MySqlHelper(ctx.applicationContext)
                }
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
/*        val createTable= "CREATE TABLE " + TABLE_NAME + "(" +
            COL_ID + " INTEGER PRIMARY KEY UNIQUE AUTOINCREMENT, " +
            COL_NAME + " VARCHAR(256), " +
            COL_PHOTO + " VARCHAR(256))" // also BLOB*/
            val createTable = "CREATE TABLE user_info (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, name VARCHAR(256), photo VARCHAR(256), faction TINYINT, time LONG)"
        db.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun delete(deleteId: Int){
        val db = this.writableDatabase
        when (deleteId) {
            -1 -> {
                db.delete(TABLE_NAME, null, null)
            }
            else -> {
                db.delete(TABLE_NAME, "id=$deleteId", null)
            }
        }
        db.close()
    }

    fun insert(card: Biocard) {
        val db = this.writableDatabase
        val cv = ContentValues()

        //cv.put(COL_ID, card.id)
        cv.put(COL_NAME, card.name)
        cv.put(COL_PHOTO, card.photo)
        cv.put(COL_FACTION, card.faction)
        cv.put(COL_TIME, card.time)

        val result = db.insert(TABLE_NAME, null, cv)
        if (result == (-1).toLong()) {
            //Toast.makeText(context, "插入失败", Toast.LENGTH_SHORT).show()
        }
        else {
            //Toast.makeText(context, "插入成功", Toast.LENGTH_SHORT).show()
        }
    }

    fun query(qId: Int?, qName: String?, qSort: String?, qFaction: Int?): ArrayList<Biocard> {
        val db = this.writableDatabase
        val list = ArrayList<Biocard>()
        val cursor: Cursor = when (qId) {
            null -> {  //搜索框
                if (qFaction == 0) {
                    db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $COL_NAME LIKE '%' || ? || '%' ORDER BY $qSort", arrayOf(qName))
                } else {
                    db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $COL_NAME LIKE '%' || ? || '%' AND $COL_FACTION = ? ORDER BY $qSort", arrayOf(qName, qFaction.toString()))
                }
            }

            -1 -> db.rawQuery("SELECT * FROM $TABLE_NAME ORDER BY $qSort", null)  //初始化展示所有数据

            else -> db.rawQuery("SELECT * FROM $TABLE_NAME WHERE $COL_ID=?", arrayOf(qId.toString()))   //cardDetail
        }

        if (cursor.moveToFirst() && cursor.count >= 1) {
            do {
                val id = cursor.getInt(cursor.getColumnIndex(COL_ID))
                val name = cursor.getString(cursor.getColumnIndex(COL_NAME))
                val photo = cursor.getString(cursor.getColumnIndex(COL_PHOTO))
                val faction = cursor.getInt(cursor.getColumnIndex(COL_FACTION))
                val time = cursor.getLong(cursor.getColumnIndex(COL_TIME))

                val card = Biocard(id, name, photo, faction, time)
                list.add(card)
            } while (cursor.moveToNext())
        }

        cursor.close()
        db.close()
        return list
    }

    fun update(card: Biocard) {
        val db = this.writableDatabase
        val cv = ContentValues()

        cv.put(COL_NAME, card.name)
        cv.put(COL_PHOTO, card.photo)

        val returnVal = db.update(TABLE_NAME, cv, "$COL_ID=${card.id}", null)
        if (returnVal >= 1) {
            Toast.makeText(context, "更新成功", Toast.LENGTH_SHORT).show()
        }
        else {
            Toast.makeText(context, "更新失败", Toast.LENGTH_SHORT).show()
        }

        db.close()
    }
}

// Access property for Context
val Context.database: MySqlHelper
    get() = MySqlHelper.getInstance(applicationContext)
