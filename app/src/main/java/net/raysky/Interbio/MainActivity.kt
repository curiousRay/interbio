package net.raysky.Interbio

import android.content.ClipData
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import net.raysky.Interbio.adapter.CardAdapter
import net.raysky.Interbio.data.Biocard
import net.raysky.Interbio.data.MySqlHelper
import net.raysky.Interbio.data.Preference
import java.lang.System.currentTimeMillis
import java.util.*

class MainActivity : AppCompatActivity() {
    private var cardList = ArrayList<Biocard> ()
    private var qName = ""
    var prefOrientation: Int by Preference(this, "prefOrientation", 0)
        // 0竖1横
    var prefSort: String by Preference(this, "prefSort", "time DESC")
    // time DESC; time ASC; name DESC; name ASC
    var prefFaction: Int by Preference(this, "prefFaction", 1)
    // 0 -> enl+res; 1 -> enl; 2 -> res

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        toolbar.setOnClickListener { Toast.makeText(this, "Toolbar title clicked", Toast.LENGTH_SHORT).show() }

        recyclerView.layoutManager = LinearLayoutManager(this)

        /*val cardList = ArrayList<Biocard> ()
        cardList.add(Biocard(1, "namex", "photox"))
        cardList.add(Biocard(2, "namey", "photoy"))
        cardList.add(Biocard(3, "namez", "photoz"))
        val adapter = CardAdapter(cardList)*/

        loadData(this, -1, null, prefSort, prefFaction) //初始加载时使用sharedpreference保存的过滤条件

        // TODO: 更好的方式是通过用户打勾选择的过滤条件，执行适当的sql语句，利于减少内存消耗。从而result即为cardList
        delete.setOnClickListener {
            MySqlHelper.getInstance(this)
                .delete(-1)
            loadData(this, -1, null, prefSort, prefFaction)
        }
        insert.setOnClickListener {
            val helper = MySqlHelper.getInstance(this)
            helper.insert(Biocard(1, randomString(6), "photoxxx", cofoxRandom(1, 3), currentTimeMillis()))

            loadData(this, -1, null, prefSort, prefFaction)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        toolbar.overflowIcon = getDrawable(R.drawable.ic_filter_list_black_24dp) // 右边的menu按钮
        this.configSv(menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        queryFilter(this, item!!)

        return super.onOptionsItemSelected(item)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        if (newConfig?.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            prefOrientation = 1
            Preference(this, "orientation", 1)
        } else if (newConfig?.orientation == Configuration.ORIENTATION_PORTRAIT
            || newConfig?.orientation == Configuration.ORIENTATION_UNDEFINED){
            prefOrientation = 0
        }
        super.onConfigurationChanged(newConfig)
    }

    private fun loadData(ctx: Context, qId: Int?, qName: String?, prefSort: String, prefFaction: Int) {
        val helper = MySqlHelper.getInstance(ctx)
        cardList = helper.query(qId, qName, prefSort, prefFaction) // -1代表查询所有
        recyclerView.adapter = CardAdapter(ctx, cardList)
        if (cardList.size == 0) {
            //Toast.makeText(this, "没有记录", Toast.LENGTH_SHORT).show()
            recyclerView.visibility = View.GONE
            no_card_tip.visibility = View.VISIBLE
        } else {
            recyclerView.visibility = View.VISIBLE
            no_card_tip.visibility = View.GONE
        }
    }

    private fun configSv(menu: Menu?) {
        menuInflater.inflate(R.menu.menu, menu) // 将 menu 内容，放到 ActionMenuView中
        val searchItem = menu!!.findItem(R.id.search)
        val sv = searchItem.actionView as SearchView
        if (prefOrientation == 1) sv.maxWidth = Int.MAX_VALUE
        sv.imeOptions = EditorInfo.IME_FLAG_NO_EXTRACT_UI
        val svPlate: View = sv.findViewById(R.id.search_plate) // 去除搜索框下划线
        svPlate.background = getDrawable(R.color.TRANSPARENT)


        sv.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                return false
            }
            override fun onQueryTextChange(s: String): Boolean {
                qName = s
                loadData(this@MainActivity, null, s, prefSort, prefFaction)
                return false
            }
        })

        sv.setOnSearchClickListener {
        }
        sv.setOnCloseListener (object: SearchView.OnCloseListener {
            override fun onClose(): Boolean {
                return false
            }
        })
    }

    private fun queryFilter(context: Context, item: MenuItem) {
        item.isChecked = !item.isChecked

        when (item.itemId){
            R.id.byDateNewer -> prefSort = "time DESC"
            R.id.byDateOlder -> prefSort = "time ASC"
            R.id.byNameAZ -> prefSort = "name ASC"
            R.id.byNameZA -> prefSort = "name DESC"
            R.id.factionAll -> prefFaction = 0
            R.id.factionENL -> prefFaction = 1
            R.id.factionRES -> prefFaction = 2
        }

        loadData(context, null, qName, prefSort, prefFaction)
    }

    private fun randomString(length: Int): String {
        val alphaNumeric = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        return alphaNumeric.shuffled().take(length).joinToString("").toLowerCase()
    }

    private fun cofoxRandom(min: Int = 0, max: Int): Int {  //取值区间：(min, max)
        var t_min = min
        var t_max = max
        if (t_min > t_max) {
            val temp: Int
            temp = t_min
            t_min = t_max
            t_max = temp
        }
        val random = (Math.floor(Math.random() * (t_max - t_min))).toInt() + t_min

        return random
    }
}
