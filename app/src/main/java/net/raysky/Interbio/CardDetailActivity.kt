package net.raysky.Interbio

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_card_detail.*
import net.raysky.Interbio.data.MySqlHelper

class CardDetailActivity: AppCompatActivity() {

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_detail)
        setSupportActionBar(cardDetailToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val intent = intent
        val selectedId = intent.getIntExtra("selectedId", -1)
        textView.text = selectedId.toString()
        val helper = MySqlHelper.getInstance(this)
        val result = helper.query(selectedId, null, null, null)
        textView2.text = result.toString()
    }
}
