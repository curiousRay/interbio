package net.raysky.Interbio.adapter

import android.content.Context
import android.content.Intent
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import net.raysky.Interbio.CardDetailActivity
import net.raysky.Interbio.R
import net.raysky.Interbio.data.Biocard

class CardAdapter(val context: Context, val cardList: ArrayList<Biocard>) : RecyclerView.Adapter<CardAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_card, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cardList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val user : Biocard = cardList[p1]

        p0.textViewName.text = user.name + "#id=" + user.id + "#faction=" + user.faction
        p0.textViewPhoto.text = user.photo
        p0.imageView.setImageResource(R.drawable.bg)
        p0.imageView.setOnClickListener{
            p0.textViewName.setTextColor(0x66FF0000)   //前两位是透明度
        }
        p0.itemView.setOnClickListener {
            //p0.textViewName.text = user.id.toString() //将user.id传至cardDetail活动以便再次query

            val intent = Intent(context, CardDetailActivity::class.java)
            intent.putExtra("selectedId", user.id)
            context.startActivity(intent)
        }

    }

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName = itemView.findViewById(R.id.textView5) as TextView
        val textViewPhoto = itemView.findViewById(R.id.textView3) as TextView
        val imageView = itemView.findViewById(R.id.imageView4) as ImageView
    }
}